# Cassandra Diagram

Create cluster diagrams from the output of `nodetool status`.

## Usage

    $ nodetool status -r | ./cassandra_diag.py > c.dot && dot -Tsvg c.dot -o c.svg

## Requirements

You need graphiz (`apt install graphviz` on Debian systems).
