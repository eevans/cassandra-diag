#!/usr/bin/python3

"""
Usage: nodetool status -r | ./cassandra_map.py > c.dot && dot -Tsvg c.dot -o c.svg
"""


import fileinput
import re
import textwrap


HOST_RE = re.compile(
    r"^.{2}\s+(?P<host>[a-zA-z0-9\-\.]+)\s+(?P<load>[0-9\.]+\s[a-zA-z]+)\s+(?P<tokens>\d+)\s+"
    "(?P<owns>[0-9\.]+%)\s+(?P<token>[a-z0-9\-]+)\s+(?P<rack>[a-zA-Z0-9_\-])+$"
)


def normalize_hostname(hostname):
    if hostname.endswith(".eqiad.wmnet") or hostname.endswith(".codfw.wmnet"):
        return hostname[:-12]
    return hostname


def print_dot(nodes):
    print(textwrap.dedent("""\
    digraph G {
        fontname="Helvetica,Arial,sans-serif"
        node [fontname="Helvetica,Arial,sans-serif"]
        edge [fontname="Helvetica,Arial,sans-serif"]
    """))
            
    for data_center in nodes:
        print('    subgraph cluster_{} {{'.format(data_center))
        print('        label = {};'.format(data_center))
        print('        style=filled;')
        print('        color=black;')
        print('        fillcolor="#dddddd";')
        print('        node [style=filled,color=white];')
    
        for rack in nodes[data_center]:
            print('        subgraph cluster_{} {{'.format(rack))
            print('            label = "{}";'.format(rack))
            print('            style=filled;')
            print('            color=lightgrey;')
            print('            fillcolor="#efefef";')
            print('            node [style=filled,shape=box,color=black,fillcolor=white];')
            print('            edge [style=invis];')
            print('            {};'.format(" -> ".join(f'"{w}"' for w in nodes[data_center][rack])))
            print('        }')
    
        print('     }')
        
    print('}')


def parse_input():
    current_datacenter = None
    nodes = {}

    for line in fileinput.input():
        m = re.match(r"^Datacenter: (?P<datacenter>\w+)$", line)
        if m:
            current_datacenter = m.group("datacenter")
            continue
    
        if not current_datacenter:
            continue
    
        m = HOST_RE.match(line)
        if m:
            rack = m.group("rack")
            if not current_datacenter in nodes:
                nodes[current_datacenter] = {}
            if not rack in nodes[current_datacenter]:
                nodes[current_datacenter][rack] = []
            nodes[current_datacenter][rack].append(normalize_hostname(m.group("host")))
            #print(m.group("host"), m.group("load"), m.group("tokens"), m.group("owns"), m.group("token"), m.group("rack"))

    return nodes


if __name__ == '__main__':
    nodes = parse_input()
    print_dot(nodes)
